# Recording function calls that take input args.
Expected calls:
    "TakesNoArg"
    "Takes8bitArg", 0x12
    "TakesTwo8bitArgs", 0xaa, 0xbb
    "Takes16bitArg", 0x1234
    "TakesGStringPtr", the expected message
Actual calls:
    "TakesNoArg"
    "Takes8bitArg_vStub", 0x12
    "TakesTwo8bitArgs", 0xcc, 0xdd
    "Takes16bitArg", 0x1234
    "TakesGStringPtr", the actual message received

---Why it failed---
Why it failed: Call #2: expected 'Takes8bitArg', was 'Takes8bitArg_vStub'. Call #3: expected '0xaa', was '0xcc'. Call #3: expected '0xbb', was '0xdd'. Call #5: expected 'the expected message', was 'the actual message received'. 