# Table of Contents
- [Repo links](#markdown-header-repo-links)
- [The basic idea](#markdown-header-the-basic-idea)
- [Understanding the pointer chain data structure](#markdown-header-understanding-the-pointer-chain-data-structure)
    - [Accessing the recorded argument at the end of the chain](#markdown-header-accessing-the-recorded-argument-at-the-end-of-the-chain)
    - [Parsing the chain](#markdown-header-parsing-the-chain)
- [Replace casted pointer chains with readable code](#markdown-header-replace-casted-pointer-chains-with-readable-code)
    - [Accessing recorded function calls](#markdown-header-accessing-recorded-function-calls)
    - [Accessing recorded input arguments for a given recorded call](#markdown-header-accessing-recorded-input-arguments-for-a-given-recorded-call)
- [Handling arbitrary datatypes with function pointer members](#markdown-header-handling-arbitrary-datatypes-with-function-pointer-members) - [Expected DOF and DOF Stubbed](#markdown-header-expected-dof-and-dof-stubbed)
    - [C idiom for arbitrary datatypes](#markdown-header-c-idiom-for-arbitrary-datatypes)
    - [My problem](#markdown-header-my-problem)
    - [My solution](#markdown-header-my-solution)
        - [Destroy shows how datatype is encoded by assigning a function pointer](#markdown-header-destroy-shows-how-datatype-is-encoded-by-assigning-a-function-pointer)
        - [How the definition of Destroy depends on datatype](#markdown-header-how-the-definition-of-destroy-depends-on-datatype)
        - [A clean way to assign datatype-specific definitions in a RecordedArg](#markdown-header-a-clean-way-to-assign-datatype-specific-definitions-in-a-recordedarg)
- [Expected DOF and DOF Stubbed for various types of DOF](#markdown-header-expected-dof-and-dof-stubbed-for-various-types-of-dof)
    - [A DOF that takes no arguments](#markdown-header-a-dof-that-takes-no-arguments)
    - [A DOF that takes a byte](#markdown-header-a-dof-that-takes-a-byte)
    - [A DOF that takes a pointer to heap memory](#markdown-header-a-dof-that-takes-a-pointer-to-heap-memory)

---e-n-d---

# The basic idea for recording calls
- I want to store two lists of *recorded function calls*.
    - `expected_calls`: a list of *calls expected* by the mockist test
    - `actual_calls`: a list of *calls actually made* by the *FUT*

- Each *list* is a chain of `GList` structs
    - The `GList` struct is three pointers:
        - `data` is a pointer to whatever I want to store
        - `next` is a pointer to the next `GList` struct
        - `prev` is a pointer to the previous `GList` struct
    - All `GList` structs are allocated on the heap.
    - Heap allocation is handled by the `g_list_append` function.

- Each *list* begins life as nothing more than a pointer to a `GList`.
    - The *list* points to `NULL` until the first function call is appended.
    - This allocates a `GList` on the heap.
    - This first `GList` has no `next` or `prev` `GList` blocks, so those pointers
      are `NULL`.
    - The `data` member points to the type of thing my *list* stores.
    - My *list* stores *recorded function calls*, so the `data` member points to
      a `RecordedCall`.

- The `RecordedCall` datatype is a struct of two pointers:
    - `name` is the name of the function that was called. It is a pointer to a
      *string literal* (a `char const *`) in *static-memory*.
    - `inputs` is another *list*, this time a list of the *recored input
      arguments*. It is a pointer to a `GList`.
- I could have made `name` a `GString`, but since I will never change it, this
  does not seem necessary. Leaving `name` as a simple *string literal* means
  that C handles all aspects of the memory management for me. I do not care
  about consuming *static-mem* because this will only ever run as a unit test on
  my development system.

## How the call lists grow
- Before the first *recorded function call* is appended, `expected_calls` and
  `actual_calls` point to `NULL`.
    - The unit test calls `Expect_DOF` for each *DOF*. This appends a
      `RecordedCall` to the `expected_calls` list.
    - After setting all the expectations, the unit test calls the *FUT*.
    - The *FUT* calls the `DOF_Stubbed`. This appends a `RecordedCall` to the
      `actual_calls` list for each *DOF*.
    - `expected_calls->data` and `actual_calls->data` both now point to the
      first *recorded function call*.
## How input lists grow
- Each *recorded function call* has its own list of inputs.
    - Each input is a *recorded input argument*.
    - If a *recorded function call* has no input arguments, then `inputs` is
      `NULL`, just as `expected_calls` was `NULL` until the first *function
      call* was appended.
    - If a *recorded function call* has input arguments, the arguments are
      appended. Now `inputs->data` points to the first *recorded input
      argument*.
## Working with lists of lists
- Note the similarity between `inputs` and `expected_calls`. This is because
  they are the same datatype: *a pointer to a `GList`*.
    - See section [Accessing recorded function calls][rec-calls] for a
      discussion on read and write access to `expected_calls->data`.
[rec-calls]: #markdown-header-accessing-recorded-function-calls
    - See section [Accessing recorded input arguments for a given recorded call][rec-args]
      for a discussion on read and write access to
      `inputs->data`.
[rec-args]: #markdown-header-accessing-recorded-input-arguments-for-a-given-recorded-call
    - See section [Replace casted pointer chains with readable code][ptr-chains]
      for suggestions on refactoring unreadable pointer chains. A hard-to-read
      pointer-chain is a good way to catch code that needs to be refactored.
      There are simple patterns to follow to do this refactoring, based on the
      specific pointeir chain and whether the pointer itself requires read
      access or write access.
[ptr-chains]: #markdown-header-replace-casted-pointer-chains-with-readable-code

# Understanding the pointer chain data structure
The mock object looks like a tree. Literally, when you draw it, it is a tree.
Any single path along that tree is a pointer chain.

## Chain concept
Here is a high-level picture of the chain:
```c
Mock -> list of RecordedCalls -> list of RecordedArgs -> pArg
```

But both of those lists have an added level of indirection because they are
linked lists, so they must use a pointer to hold elements of arbitrary data
type. The data pointer is called `data`. With more detail then:
```c
Mock -> expected_calls -> data -> inputs -> data -> pArg
```

At this point it is confusing: I've dropped the helpful nouns `RecordedCalls`
and `RecordedArgs` and I've added these ambiguous `data` things. The compiler is
as confused you are. The `data` pointer can point to anything, so it needs to be
cast as a pointer to a specific datatype. Here is the final version of the
chain:

```c
((RecordedArg *)((RecordedCall *)mock->expected_calls->data)->inputs->data)->pArg
```

That beast is a pointer to one argument value recorded for one function call
recorded on the `expected_calls` list.

## Accessing the recorded argument at the end of the chain
To actually access the value of the argument, one more step:
```c
*((uint8_t *)((RecordedArg *)((RecordedCall *)mock->expected_calls->data)->inputs->data)->pArg)
```
In the above example, the datatype of the *recorded argument* is `uint8_t`. Any
non-pointer type of argument follows this same format. `pArg` points to a copy
of the non-pointer argument. `pArg` is a void pointer, so it must be cast to the
correct type before it can be dereferenced. To do this, the entire chain is cast
to the datatype. Then the entire chain is dereferenced.

If the recorded argument is a *pointer* type instead, then `pArg` holds a
pointer of the same datatype. `pArg` points to a copy of the data at a different
location on the heap. So instead of dereferencing with the leading `*`, the
`pArg` is cast to the correct pointer type and then treated as if it is that
pointer type:
```c
((GString *)((RecordedArg *)((RecordedCall *)mock->expected_calls->data)->inputs->data)->pArg)->str
```
In this example, the *recorded argument* is a pointer to a `GString`. A copy of
the `str` member of the `GString` was made and placed in a new `GString` on the
heap. `pArg` points at the new `GString`, so `pArg->str` accesses that copy of
the `str` member.

## Parsing the chain
Here is how to parse the beast:
```c
                                                        *((uint8_t *)
                                    ((RecordedArg *)
(   (RecordedCall *)
    mock->expected_calls->data )
                                    ->inputs->data)     ->pArg)
```
- The `data` in an `expected_calls` list points to a `RecordedCall`.
- The `data` in an `inputs` list points to a `RecordedArg`.
- A `RecordedArg` has a `pArg` that is a pointer to the arg value.

- `GList` data can point to anything, so when it is dereferenced, it must be
  cast to the appropriate pointer, in this case the `(RecordedCall *)` and the
  `(RecordedArg *)`.
- Similarly, the `pArg` is a pointer to void. When it is dereferenced, it must
  be cast to the appropriate datatype. But before it can be dereferenced, it
  must be cast to the appropriate pointer. This is a C idiom.

Being able to draw the beast is essential for writing code.
Being able to read and write this beast is useful for checking values in
`gdb`.
But this beast should never show up in practice in the project code. The
following sections discuss working with the beast.

# Replace casted pointer chains with readable code
- Three ways to replace pointer chains with readable code:
    1. split the code into functions that work on a single pointer in the chain
    2. within a function, create a local copy of the `GList *` to shorten the pointer chain
    3. create accessors that return a child pointer

- I use the first two methods in `single-file-example.c`.
- In a larger codebase, the third method makes sense to avoid code repetition
  from the second method: instead of every function defining the same locals,
  define once as a function and use the `f(g(x))` style of composition.

- Example of a pointer chain that accesses one `RecordedArg` in a
  `RecordedCall`:
```c
(RecordedArg *)((RecordedCall *)mock->expected_calls->data)->inputs->data
```
- This style is unreadable. I only use this in `gdb`.
- Make it readable by:
    - splitting up functions based on which part of the pointer chain they need
    - and also creating locals to shorten the pointer chain
- Example: `PrintAllCalls`
    - `PrintAllCalls` requires access to:
        - `RecordedCall` to print the call name
        - `RecordedArg` to print the input arguments

`PrintAllCalls` takes `(Mock_s *self)`, which is the root pointer in the chain.
It starts by chopping off the root.
```c
GList *expected_calls   = self->expected_calls;
```
To work with a specific `RecordedCall` within a loop, it creates local loop
variable `this_call`:
```c
RecordedCall *this_call = (RecordedCall *)expected_calls->data;
```
That gives `this_call->name`, a *highly-readable* accessor to the *name of the
recorded function call*. Similarly, it gives `this_call->inputs` to access the
input arguments of this recorded function call.

Thus far, these are all examples of creating locals to shorten the pointer
chain. Here is an example of splitting the function based on the part of the
chain it accesses. `PrintAllInputs` is a separate function created for
printing `inputs`.
```c
static void PrintAllInputs(GList *inputs)
{
    // do stuff with a call's inputs
}
void PrintAllCalls(Mock_s *self)
{
    ...  // do stuff to each call, but then to work with a call's inputs:
    PrintAllInputs(this_call->inputs);
    ...
}
```
And within `PrintAllInputs` I use the same locals trick as before. Looping
through the input arguments, I work with a specific argument by creating
`this_arg`:
```c
RecordedArg *this_arg = (RecordedArg *)inputs->data;
```

## Accessing recorded function calls
This is a pointer to a recorded function call:
```c
(RecordedCall *)mock->expected_calls->data
```
Like any pointer, a copy of this pointer provides write access to what it points
to, but it only provides read access to the pointer itself! This is no different
from anything else in C, but it gets confusing with pointers, so it is worth
restating.

*A copy of this pointer only provides read access to itself.*

*To write to the pointer itself, work one level up in the pointer chain.*

This is an important point because the whole idea is that the list of pointers
grows. As this happens, the head of the list is moved around as needed by the
memory management mechanics under the hood of the `GList` implementation. So it
is necessary to write to the pointer itself when performing operations that
might cause the pointer to change.

*A function that needs to write to the pointer
needs to work with the pointer one level up in the pointer chain.*

This is why the `mock` object is needed by functions that work with the lists of
calls. The `expected_calls` list and `actual_calls` list are being modified.

## Accessing recorded input arguments for a given recorded call
This is a pointer to a recorded input argument:
```c
(RecordedArg *)((RecordedCall *)mock->expected_calls->data)->inputs->data
```
Same idea:

*A function that needs to write to the pointer
needs to work with the pointer one level up in the pointer chain.*

This is why a `RecordedCall` object is needed by functions that change a
given call's list of inputs. The `inputs` list is modified.

# Handling arbitrary datatypes with function pointer members
The C idiom to write code that works with an arbitrary datatype is to create a
function pointer that tells client code how to do the datatype-specific stuff.

If the client code is a function, the function pointer becomes an input argument
to that function.

## C idiom for arbitrary datatypes
An example of this style is `qsort`:
```c
void qsort(
    void *base,     // THE ARRAY TO SORT (the address of its initial element)
    size_t nel,     // number of elements               in the array to sort
    size_t width,   // size in bytes of each object     in the array to sort
    int (*compar)(const void *, const void *) // how to compare two elements
    );
```
The first parameter of `qsort` is a *void-pointer*. This generalizes `qsort` to
sort an array of any datatype.

The last parameter of `qsort` is a *function pointer* typedef. Whatever function
name is passed into this argument, `compar` will alias to that function. A
comparison function is defined for each possible array element datatype. New
function definitons are created as the need for handling additional datatypes
arises. There is no need to modify old code because the codebase handles a new
datatype.

## My problem
This style works when you know the datatype *when you operate on
it*. My problem was that I knew the datatype *only at the moment the input
argument was recorded*.

I only knew what the datatype of the argument when I recorded it in the list of
inputs. But I needed to access this recorded input argument at a later time
while walking a list of the input arguments in a loop. There was no way for me
to rediscover the datatype when I wanted to access the data because *C cannot
introspect datatype*. The *GLib* `GVariant` datatype, a struct intended to hold
an arbitrary datatype, solves this using string names saved with the data.
I have not tried using a `GVariant` but I assume its library functions use this
string to figure out what the datatype is.

## My solution
The `GVariant` solves a more general problem. But in my case, I already know
exactly all of the operations to be perfomed on my *recorded input argument*. My
solution was to look into the future and imagine how the recorded input argument
will be used. Instead of using a string identifier for taking appropriate action
later, I assign every operation to the definition appropriate to the datatype of
the recorded argument.

For each function that needs to act on a *recorded input argument*, there is a
definition for each possible datatype. This is identical to the `qsort` example
above. My twist on this is to assign the function pointer for that use *at the
moment the input argument is recorded*.

Recording the input argument means:

- allocating memory on the heap to hold a copy of the argument
- allocating memory to hold a struct of pointers:
    - The first pointer, `pArg`, points to the copy of the argument.
    - The other pointers are function pointers assigned to the definitions
      appropriate for this datatype.

### Destroy shows how datatype is encoded by assigning a function pointer
For example, the mock object is a tree of pointers to many memory locations on
the heap. To destroy the mock object, I have to walk the entire tree, find the
end of each pointer chain, start deallocating from that end, then work my way
back, deallocating as I go, finally ending at the original mock object itself,
deallocating the memory that holds its three pointers.

Restating this in more of a pseudo-code form:

- I walk the lists of recorded calls: `expected_calls` and `actual_calls`
    - for each *recorded call*, I walk the list of inputs
        - for each *recorded argument*, I call a destroy method for each
          recorded input argument.

Destroying recorded calls is easy. All recorded calls are the same type:
`RecordedCall`, so I always cast the `data` member of the `GList` as a
`RecordedCall`. Same goes for the `RecordedArg`: the `data` member of the
`GList` that holds the recorded arguments is always cast as a `RecordedArg`.

But the arguments themselves are tricky. The `pArg` in the `RecordedArg` is a
void pointer. The datatype it points to is whatever datatype is stored in that
`RecordedArg`. When walking the tree, I do not know the argument type, so I
cannot choose the correct `Destroy` function. Of course I face the same problem
any time I need to know the datatype of the argument. The only time I know the
datatype is when I first create the `RecordedArg`.

Instead, I assign `Destroy` to point at the correct definition when the input
argument is recorded. As I iterate, each *recorded argument* already has the
correct definition of `Destroy` associated with it. I can simply call its
`Destroy` function.

### How the definition of Destroy depends on datatype
The `data` member of a `GList` is a pointer to void. The `pArg` in the
`RecordedArg` is also a pointer to void.

If `data` or `pArg` point to a non-pointer, a simple `free`
works, but I need to first cast the pointer to the correct datatype. This tells
the compiler how many bytes to deallocate. `free` does not normally require a
cast: the compiler figures out how many bytes to free from the datatype of the
pointer that is passed in. But like any other function, if the datatype is a
pointer to void, a cast is necessary. So a datatype-specific definition of
`Destroy` is needed, even in this simple case where the `Destroy` is just a call
to `free`. In the case of `pArg`, this datatype-specific definition is assigned
to the `Destroy` function pointer in its particular instance of `RecordedArg`.

If `data` or `pArg` points to *yet another pointer to a struct on the heap*,
then I need to deallocate with a `free` that is specific to that struct pointer.
For example, if the recorded input argument is a `GString` pointer, call
`g_string_free` on the input argument.

### A clean way to assign datatype-specific definitions in a RecordedArg
The function pointers must all be declared in the `RecordedArg` typedef. If I
add a new function to this list, how can I be sure clients of `RecordedArg` know
about it? If a client does not assign a definition to the function pointer, it
will point at `NULL` and the executable will seg fault when the function is
called.

The safest way is to hide *the addition of functions* from client code. The
client has to know the functions exist to use them, but the client should not
care if more functions are added, i.e., the client should not have to revise all
calls to `RecordedArg_new()`.

My solution is to create a `Setup` function that assigns all function pointers.
The client calls `RecordedArg_new(SetupRecord_blah)`, passing the definition of
`SetupRecord` specific to the datatype of the recorded argument. As new
functions are added, I simply add the assignment of those new function to each
datatype-specific definition of `SetupRecord`.

Now the client need only know the declaration of the datatype-specific
definitions of `SetupRecord`. The declarations of the datatype-specific
definitions of the other functions, including future functions I have not
thought of yet, all remain hidden in the libs `.c` file.

So `RecordArg_new()` is called with one input, a function that assigns
datatype-specific functions to this record, thus saving the datatype of the
argument being recorded. Storing the value of the argument is separate from this
instantiation of the record.

Similarly, `RecordCall_new()` is called with one input, a string literal holding
the name of the function call, thus saving the name of the function being
recorded. Storing any input arguments required by this function call is separate
from this instantiation of the record.

In other words, just as a `RecordedArg` must have, at a minimum, a datatype, a
`RecordedCall` must have, at a minimum, a call name.

# Expected DOF and DOF Stubbed for various types of DOF
## A DOF that takes no arguments
Here is an example of mocking a function that takes no arguments.
```c
RecordedCall * Mock_TakesNoArg(void)
{
    char const *call_name = "TakesNoArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    return record_of_call;
}
void Expect_TakesNoArg(void)
{
    RecordExpectedCall(mock, Mock_TakesNoArg());
}
void TakesNoArg_Stubbed(void)
{
    RecordActualCall(mock, Mock_TakesNoArg());
}
```
The `Expect_DOF` and `DOF_Stubbed` create identical call records and only
differ in which list they save their calls to. All of the call-record-generation
code is placed in the `Mock_DOF` function. This avoids the possibility of the
two being out of sync with regards to the name of the *DOF*.

Selecting which list to record to cannot be passed as a parameter because the
`DOF_Stubbed` must have a function that matches with the function it is
stubbing!

## A DOF that takes a byte
Here is an example of mocking a function that takes a byte.
```c
RecordedCall * Mock_Takes8bitArg(uint8_t My8BitArg)
{
    char const *call_name = "Takes8bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_Takes8bitArg(uint8_t My8BitArg)
{
    RecordExpectedCall(mock, Mock_Takes8bitArg(My8BitArg));
}
void Takes8bitArg_Stubbed(uint8_t My8BitArg)
{
    RecordActualCall(mock, Mock_Takes8bitArg(My8BitArg));
}
```
## A DOF that takes a pointer to heap memory
This is an interesting twist. Up until now, all input arguments are passed by
value and the `Mock_DOF` allocates heap memory, copies the value there, and
stores the pointer to the heap in `pArg`.

But if the input argument is itself a pointer, clearly this makes no sense. We
are not interested in the value of the pointer, we want what the pointer points
to. It is tempting to simply alias the pointer and be done. But then who is
responsible to manage the memory? It would be messy if the tree-walking
`Mock_Destroy()` had to keep track of whether or not it was responsible for
deallocating the memory pointed to at the end of each chain.

It is cleaner to treat this exactly like the other cases. Then `Mock_Destroy()`
does not have to handle special cases where the memory is deallocated by the
client. It consumes a little more memory, but it keeps a consistent approach at
the high level, and that makes the code easier to maintain.

The high-level view is that the `pArg` points to a copy of the input argument,
whether that argument is a pointer to the heap or a simple non-pointer. If the
input argument is itself a pointer to the heap, first copy what that pointer
points to. Now the `RecordedArg` points to its own copy of the data, and the
`mock` object manages that memory independently of how the client code manages
the original.

```c
RecordedCall * Mock_TakesGStringPtr(GString *pGString)
{
    char const *call_name = "TakesGStringPtr";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_GString);
    record_of_arg1->pArg = (void *)g_string_new(pGString->str);
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
```

This is actually one line shorter than the previous examples because
`g_string_new` takes an initial value for the string. That is a good spot to
access the value pointed to by the input argument.

Again, the `Expect_DOF` and `DOF_Stubbed` functions are just boilerplate:
```c
void Expect_TakesGStringPtr(GString *pGString){
    RecordExpectedCall(mock, Mock_TakesGStringPtr(pGString));
}
void TakesGStringPtr_Stubbed(GString *pGString){
    RecordActualCall(mock, Mock_TakesGStringPtr(pGString));
}
```

As mentioned, the expectation is that the client does its own memory management
on the objects it is calling. Here is an example of such memory management in
the test code that calls the `Expect_DOF`:
```c
    // Function calls that do not require memory management by the client.
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    // Passing an object requires memory management by the client.
    // Memory is allocated on the heap:
    GString *pExpectedMsg = g_string_new("the expected message");
    Expect_TakesGStringPtr(pExpectedMsg);
    // The heap memory is deallocated:
    g_string_free(pExpectedMsg, true);
```
The memory is deallocated as soon as the call to `Expect_DOF` is made. This is
to avoid the possibility of forgetting to deallocate it later. In an actual
test, both the allocation and deallocation could go into a `setUp` and
`tearDown` if the objects values are known at the start of the test. And there
is no danger in deallocating immediately because `Expect_DOF` makes its own copy
of the object data.


Reviewing what is happening for pointer-to-heap arguments:

1. access the value on the heap pointed to by the pointer argument
2. allocate heap memory to hold the datatype pointed to
3. copy the value from the original heap location to the new heap location
4. store the new pointer to the heap as for any other `pArg`

Any proper object has a `new` method, so this is easier than it sounds.
And of course the object has some way to access its value on the heap:

- If the struct is public, the access is through the dereferencing pointer
  notation, `->`.
- If it is an abstract data type, the API will have an accessor
  function.

In practice, the 4 steps above are one line of code. I compare the non-pointer
and pointer-to-heap versions side-by-side.

First, for arguments that are simple non-pointer types like `uint8_t`:
```c
RecordedCall * Mock_Takes8bitArg(uint8_t My8BitArg){
    ...
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);    // create record
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;              // here is the one-liner
    ...
}
```
And for arguments that point to heap memory, like a pointer to a `GString`:
```c
RecordedCall * Mock_TakesGStringPtr(GString *pGString){
    ...
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_GString);  // create record
    record_of_arg1->pArg = (void *)g_string_new(pGString->str);  // here is the one-liner
    ...
}
```

- `g_string_new` allocates the heap memory and returns a pointer to it. The
  pointer is cast as `(void *)` to match the type of `pArg`. I think clang makes
  this cast implicitly, but I prefer to make it explicit as reminder that the
  original datatype was lost at this step, but saved in the previous step by
  passing `SetupRecord_GString`.
- `->str` access the `str` member of the `GString` struct, which is where the
  actual `char` array is stored. The new `GString` is initialized with a deep
  copy of the `char` array in the original `GString`.

### Before refactoring call record generation
Here is the original code before I refactored the call-record-generation into a
single `Mock_DOF`.
```c
void Expect_Takes8bitArg(uint8_t My8BitArg)
{
    // Create a record of this call.
    char const *call_name = "Takes8bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);

    // Create records of the inputs passed to this call.
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;

    // Save the inputs to the list of inputs for this call:
    // `record_of_call->data` now aliases `record_of_arg`N
    RecordArg(record_of_call, record_of_arg1);

    // Save the call record in the appropriate list of calls:
    // `mock->expected_calls->data` now aliases `record_of_call`
    RecordExpectedCall(mock, record_of_call);
}
void Takes8bitArg_Stubbed(uint8_t My8BitArg)
{
    // Create a record of this call.
    char const *call_name = "Takes8bitArg_vStub";
    RecordedCall *record_of_call = RecordedCall_new(call_name);

    // Create records of the inputs passed to this call.
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;

    // Save the inputs to the list of inputs for this call:
    // `record_of_call->data` now aliases `record_of_arg`N
    RecordArg(record_of_call, record_of_arg1);

    // Save the call record in the appropriate list of calls:
    // `mock->actual_calls->data` now aliases `record_of_call`
    RecordActualCall(mock, record_of_call);
}
```

# Repo links
Link to this repo: https://bitbucket.org/rainbots/record-call/src/master/

Clone this repo:
```bash
git clone https://rainbots@bitbucket.org/rainbots/record-call.git
```

