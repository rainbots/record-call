#=====[ Default: build and run stand-alone .c files to view a .md output ]=====
    #;mkc / ;mkl / ;mk+ to build as usual, but cursor must be in .c file
    #...CURSOR MUST BE IN .C FILE...
    #;re to open markdown in window, ;r<Space> to close markdown window
    #;mrc / ;mrl / ;mr+ to build *and* view markdown
    #;mc to clean as usual, but cursor must be in .c file to find the .md file
    #test a recipe:
	#!make record-calls -n 2> make_record-calls_-n.md
# Swap the order of the following three targets, depending on what you want.

test-results: build/TestSuite-results.md
record-calls: build/record-calls.md

CFLAGS = -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include \
	-g -Wall -Wextra -pedantic
LFLAGS = -lglib-2.0 -lintl -L/usr/lib/glib-2.0

#=====[ record-calls ]=====
build/%.md: build/%.exe
	$^ > $@

build/record-calls.exe: src/record-calls.c build/RecordedArg.o build/RecordedCall.o build/Mock.o
	${compiler} $^ -o $@ $(LFLAGS) $(CFLAGS) \
		-I src/

build/%.o: src/%.c
	${compiler} -c $^ -o $@ $(CFLAGS) \
		-I src/

#=====[ TestSuite ]=====
lib-names := Mock RecordedCall RecordedArg ExampleCalls
lib-path := src/
build-path := build/
test-path := test/
unity-path := ${test-path}unity/
# Create the paths to the compiled lib objects.
lib-objects := $(addsuffix .o,${lib-names})
lib-objects := $(addprefix ${build-path},${lib-objects})
# Create the paths to the lib code.
lib-cfiles := $(addsuffix .c,${lib-names})
lib-cfiles := $(addprefix ${lib-path},${lib-cfiles})
lib-hfiles := $(addsuffix .h,${lib-names})
lib-hfiles := $(addprefix ${lib-path},${lib-hfiles})
# Create the paths to the lib unit test code.
lib-tests := $(addsuffix .c,${lib-names})
lib-tests := $(addprefix ${test-path}/test_,${lib-tests})
# Create the paths to the lib unit test objects.
lib-test-objects := $(addsuffix .o,${lib-names})
lib-test-objects := $(addprefix ${build-path}test_,${lib-test-objects})

${build-path}TestSuite-results.md: ${build-path}TestSuite.exe
	./$^ > $@

test-runner := ${test-path}test_runner.c
${build-path}TestSuite.exe: ${test-runner} ${lib-test-objects} ${lib-objects} ${build-path}unity.o
	${compiler} $^ -o $@ $(LFLAGS) $(CFLAGS) \
		-I ${unity-path} \
		-I ${lib-path} \
		-I ${test-path}

#=====[ Explicit rule for lib-test-objects ]=====
${lib-test-objects}: ${build-path}%.o: ${test-path}%.c
	${compiler} -c $^ -o $@ $(CFLAGS) \
		-I ${unity-path} \
		-I ${lib-path}

#=====[ Explicit rule for lib-objects ]=====
${lib-objects}: ${build-path}%.o: ${lib-path}%.c
	${compiler} -c $^ -o $@ $(CFLAGS) \
		-I ${lib-path}

${build-path}unity.o: ${unity-path}unity.c
	${compiler} -c $^ -o $@ $(CFLAGS)

#;mc
.PHONY: clean-all-builds
clean-all-builds:
	rm -f build/${file-stem}.exe
	rm -f build/TestSuite.exe
	rm -f ${lib-objects}
	rm -f ${lib-test-objects}
	rm -f build/unity.o


