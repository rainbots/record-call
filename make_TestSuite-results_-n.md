clang -c test/test_Mock.c -o build/test_Mock.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I test/unity/ \
	-I src/
clang -c test/test_RecordedCall.c -o build/test_RecordedCall.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I test/unity/ \
	-I src/
clang -c test/test_RecordedArg.c -o build/test_RecordedArg.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I test/unity/ \
	-I src/
clang -c src/Mock.c -o build/Mock.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
clang -c src/RecordedCall.c -o build/RecordedCall.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
clang -c src/RecordedArg.c -o build/RecordedArg.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
make: *** No rule to make target 'build/unity.o', needed by 'build/TestSuite.exe'.  Stop.
