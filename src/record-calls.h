#ifndef _RECORD_CALLS_H
#define _RECORD_CALLS_H

RecordedCall * Mock_TakesNoArg(void);
void Expect_TakesNoArg(void);
void TakesNoArg_Stubbed(void);

RecordedCall * Mock_Takes8bitArg(uint8_t My8BitArg);
void Expect_Takes8bitArg(uint8_t My8BitArg);
void Takes8bitArg_Stubbed(uint8_t My8BitArg);

RecordedCall * Mock_TakesTwo8bitArgs(uint8_t b1, uint8_t b2);
void Expect_TakesTwo8bitArgs(uint8_t b1, uint8_t b2);
void TakesTwo8bitArgs_Stubbed(uint8_t b1, uint8_t b2);

RecordedCall * Mock_Takes16bitArg(uint16_t My16bitArg);
void Expect_Takes16bitArg(uint16_t My16bitArg);
void Takes16bitArg_Stubbed(uint16_t My16bitArg);

RecordedCall * Mock_TakesGStringPtr(GString *pGString);
void Expect_TakesGStringPtr(GString *pGString);
void TakesGStringPtr_Stubbed(GString *pGString);

#endif // _RECORD_CALLS_H
