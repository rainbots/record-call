#include "RecordedArg.h"
#include <stdlib.h>     // malloc, free
#include <stdbool.h>    // bool, true, false

RecordedArg * RecordedArg_new(SetupRecordedArg SetupRecord)
{
    RecordedArg *out = (RecordedArg *)malloc(sizeof(RecordedArg));
    SetupRecord(out);
    return out;
}
void RecordedArg_destroy(RecordedArg *self)
{   // Deallocate this recorded arg.
    self->Destroy(self);        // Deallocate the memory `pArg` points to.
                                // `Destroy` is assigned to a function specific
                                // to the datatype of this recorded arg.
    free(self);                 // Deallocate the RecordedArg struct itself.
}

//=====[ RecordedArg: type-specific functions assigned by SetupRecord ]=====
static FreeArg              Destroy_uint8;
static FreeArg              Destroy_uint16;
static FreeArg              Destroy_GString;
static PrintArgToString     Print_uint8;
static PrintArgToString     Print_uint16;
static PrintArgToString     Print_GString;
static DoArgsMatch          Match_uint8;
static DoArgsMatch          Match_uint16;
static DoArgsMatch          Match_GString;

//=====[ RecordedArg: Define SetupRecord for different arg datatypes ]=====
void SetupRecord_uint8(RecordedArg *self)
{
    self->pArg = malloc(sizeof(uint8_t));   // Get heap-mem.
    self->Destroy = Destroy_uint8;          // How to deallocate.
    self->Print   = Print_uint8;            // How to print.
    self->Match   = Match_uint8;            // How to check if args match.
}
void SetupRecord_uint16(RecordedArg *self)
{
    self->pArg = malloc(sizeof(uint16_t));  // Get heap-mem.
    self->Destroy = Destroy_uint16;         // How to deallocate.
    self->Print   = Print_uint16;           // How to print.
    self->Match   = Match_uint16;           // How to check if args match.
}
void SetupRecord_GString(RecordedArg *self)
{
    self->pArg = malloc(sizeof(GString));   // Get heap-mem.
    self->Destroy = Destroy_GString;        // How to deallocate.
    self->Print   = Print_GString;          // How to print.
    self->Match   = Match_GString;          // How to check if args match.
}

//=====[ RecordedArg: Define Destroy for different arg datatypes ]=====
static void Destroy_uint8(RecordedArg *self)
{
    free((uint8_t *)self->pArg);
}
static void Destroy_uint16(RecordedArg *self)
{
    free((uint16_t *)self->pArg);
}
static void Destroy_GString(RecordedArg *self)
{
    g_string_free((GString *)self->pArg, true);
}

//=====[ RecordedArg: Define Print for different arg datatypes ]=====
static void Print_uint8(GString *string, RecordedArg *self)
{   // Caller provides a temporary string to store the result in.
    g_string_printf(string, "%#04x", *(uint8_t *)self->pArg);
}
static void Print_uint16(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%#06x", *(uint16_t *)self->pArg);
    //g_string_printf(string, "%d", *(uint16_t *)self->pArg);
}
static void Print_GString(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%s", ((GString *)self->pArg)->str);
}

//=====[ RecordedArg: Define Match for different arg datatypes ]=====
static bool Match_uint8(RecordedArg *arg1, RecordedArg *arg2)
{
    return *((uint8_t *)arg1->pArg) == *((uint8_t *)arg2->pArg);
}
static bool Match_uint16(RecordedArg *arg1, RecordedArg *arg2)
{
    return *((uint16_t *)arg1->pArg) == *((uint16_t *)arg2->pArg);
}
static bool Match_GString(RecordedArg *arg1, RecordedArg *arg2)
{
    int strings_do_not_match = g_strcmp0(
        ((GString *)arg1->pArg)->str,
        ((GString *)arg2->pArg)->str);
    return strings_do_not_match ? false : true;
}
