#ifndef _RECORDEDARG_H
#define _RECORDEDARG_H
#include <glib.h>       // GString, GList
#include <stdbool.h>    // bool, true, false

//=====[ mock-c RecordedArg datatype ]=====
typedef struct RecordedArg RecordedArg;
typedef void (SetupRecordedArg)(RecordedArg *self);
SetupRecordedArg     SetupRecord_uint8;
SetupRecordedArg     SetupRecord_uint16;
SetupRecordedArg     SetupRecord_GString;
typedef void (FreeArg)(RecordedArg *self);
typedef void (PrintArgToString)(GString *string, RecordedArg *self);
typedef bool (DoArgsMatch)(RecordedArg *arg1, RecordedArg *arg2);
struct RecordedArg
{
    void *pArg;    // ex: *((uint8_t *)record_of_arg->pArg) returns a uint8_t
    // Store functions for working with the value on the heap pArg points to.
    // The function pointers are assigned by a SetupRecordedArg function.
    FreeArg             *Destroy;
    PrintArgToString    *Print;
    // Anything else needed by mock-c goes here.
    DoArgsMatch         *Match;
};
RecordedArg * RecordedArg_new(SetupRecordedArg SetupRecord);
void RecordedArg_destroy(RecordedArg *self);

#endif // _RECORDEDARG_H
