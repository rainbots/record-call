#include "Mock.h"
#include <stdlib.h>     // malloc, free

struct Mock_s {
    GString *fail_msg;
    GList *expected_calls;    // The GList must be initialized to NULL.
    GList *actual_calls;    // The GList must be initialized to NULL.
};
Mock_s * Mock_new(void)
{   // Initialize all pointer members!
    Mock_s *out = (Mock_s *)malloc(sizeof(Mock_s));
    out->fail_msg = g_string_new(
        "No failures recorded. This message should never print."
        );
    out->expected_calls = NULL;
    out->actual_calls   = NULL;
    return out;
}
void Mock_destroy(Mock_s *self)
{   // Deallocate this mock struct.
    g_list_free_full(           // Deallocate each GList struct and free the
        self->expected_calls,   // RecordedCall that each data member points to.
        (GDestroyNotify)RecordedCall_destroy);
    g_list_free_full(
        self->actual_calls,
        (GDestroyNotify)RecordedCall_destroy);
    g_string_free(              // Deallocate the GString struct 
        self->fail_msg, true);  // and the message string it points to.
    free(self);  // Tabled: I cannot tell if this is deallocating the Mock_s.
}
void RecordExpectedCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of expected calls.
    self->expected_calls = g_list_append(
        self->expected_calls,
        (RecordedCall *)func_call
        );
}
void RecordActualCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of actual calls.
    self->actual_calls = g_list_append(
        self->actual_calls,
        (RecordedCall *)func_call
        );
}
void RecordArg(RecordedCall *self, RecordedArg *input_arg)
{
    self->inputs = g_list_append(
        self->inputs,
        (RecordedArg *)input_arg
        );
}

//=====[ Examples using Mock to implement mock-c functionalty ]=====
#include <stdio.h>      // printf
static void PrintAllInputs(GList *inputs)
{
    GString *printed_arg = g_string_new(
        "Error: `RecordedArg->Print` should have overwritten this message."
        );
    while (inputs != NULL)
    {
        RecordedArg *this_arg = (RecordedArg *)inputs->data;
        (this_arg->Print(printed_arg, this_arg));   // print to the GString
        printf(", %s", printed_arg->str);           // print to stdout
        //printf(", %s", ((AnyType_s *)inputs->data)->printed->str);
        inputs = inputs->next;
    }
    printf("\n");
    g_string_free(printed_arg, true);
}
void PrintAllCalls(Mock_s *self)  // Example
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    //while (expected_calls != NULL && actual_calls != NULL)
    printf("Expected calls:\n");
    while (expected_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)expected_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        expected_calls  = expected_calls->next;
    }
    printf("Actual calls:\n");
    while (actual_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)actual_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        actual_calls  = actual_calls->next;
    }
}

//=====[ Extending old mock-c ]=====
static bool Each_call_has_the_correct_number_of_inputs(Mock_s *self);
static void print_first_call_with_wrong_number_of_inputs(Mock_s *self);
static uint8_t NumberOfArgs(RecordedCall *self);
//=====[ Copied from old mock-c ]=====
static bool Call_lists_are_the_same_length(Mock_s *self);
static bool EachCallMatches(Mock_s *self);
static void print_number_of_expected_and_actual_calls(Mock_s *self);
static void print_first_unexpected_call(Mock_s *self);
static void print_first_missed_call(Mock_s *self);
static void print_all_wrong_calls(Mock_s *self);

bool RanAsHoped(Mock_s *self)
{
    // Initialize message
    g_string_printf( self->fail_msg, "Why it failed: ");
    
    if (!Call_lists_are_the_same_length(self))
    {
        print_number_of_expected_and_actual_calls(self);
        print_first_unexpected_call(self);
        print_first_missed_call(self);
    }
    if (!Each_call_has_the_correct_number_of_inputs(self))
        print_first_call_with_wrong_number_of_inputs(self);
    if (!EachCallMatches(self))
        print_all_wrong_calls(self);
    return (
        Call_lists_are_the_same_length(self) &&
        Each_call_has_the_correct_number_of_inputs(self) &&
        EachCallMatches(self)
        );
}
char const * WhyDidItFail(Mock_s *self)
{
    return self->fail_msg->str;
}

static bool Call_lists_are_the_same_length(Mock_s *self)
{
    return (
        g_list_length(self->expected_calls)
        ==
        g_list_length(self->actual_calls)
        );
}
static bool Each_call_has_the_correct_number_of_inputs(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    while (expected_calls != NULL && actual_calls != NULL)
    {
        RecordedCall *this_expected_call = (RecordedCall *)expected_calls->data;
        RecordedCall *this_actual_call   = (RecordedCall *)actual_calls->data;
        if (NumberOfArgs(this_expected_call) != NumberOfArgs(this_actual_call))
            return false;

        expected_calls  = expected_calls->next;
        actual_calls    = actual_calls->next;
    }
    return true;
}
static bool CallNamesMatch(char const *expected_name, char const *actual_name)
{
    int names_do_not_match = g_strcmp0(expected_name, actual_name);
    return names_do_not_match ? false : true;
}

static bool RecordedArgsMatch(RecordedArg *expected_arg, RecordedArg *actual_arg)
{
    //if (match functions are not the same then the types do not even match)
    if (expected_arg->Match != actual_arg->Match) return false;
    //use the match function to return true if they match
    return expected_arg->Match(expected_arg, actual_arg);
}
static bool CallInputsMatch(GList *expected_inputs, GList *actual_inputs)
{
    while (expected_inputs != NULL && actual_inputs != NULL)
    {
        RecordedArg *this_expected_arg = (RecordedArg *)expected_inputs->data;
        RecordedArg *this_actual_arg   = (RecordedArg *)actual_inputs->data;
        if (!RecordedArgsMatch(
            this_expected_arg,
            this_actual_arg)) return false;

        expected_inputs = expected_inputs->next;
        actual_inputs   = actual_inputs->next;
    }
    return true;

}
static bool EachCallMatches(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    while (expected_calls != NULL && actual_calls != NULL)
    {
        RecordedCall *this_expected_call = (RecordedCall *)expected_calls->data;
        RecordedCall *this_actual_call   = (RecordedCall *)actual_calls->data;
        if (!CallNamesMatch(
            this_expected_call->name,
            this_actual_call->name)) return false;
        if (!CallInputsMatch(
            this_expected_call->inputs,
            this_actual_call->inputs)) return false;

        expected_calls  = expected_calls->next;
        actual_calls    = actual_calls->next;
    }
    return true;
}
static void print_number_of_expected_and_actual_calls(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    uint8_t num_expected_calls = g_list_length(expected_calls);
    uint8_t num_actual_calls   = g_list_length(actual_calls);
    g_string_append_printf( self->fail_msg,
        "Expected %d calls, received %d calls. ",
        num_expected_calls, num_actual_calls
        );
}
static uint8_t NumberOfArgs(RecordedCall *self) {
    return g_list_length(self->inputs);
}
static void print_first_call_with_wrong_number_of_inputs(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    int call_number = 1;
    while (expected_calls != NULL && actual_calls != NULL)
    {
        RecordedCall *this_expected_call = (RecordedCall *)expected_calls->data;
        RecordedCall *this_actual_call   = (RecordedCall *)actual_calls->data;
        if (NumberOfArgs(this_expected_call) != NumberOfArgs(this_actual_call))
            g_string_append_printf( self->fail_msg,
                "Wrong number of args in call #%d '%s', expected %d, was %d. ",
                call_number,
                this_expected_call->name,
                NumberOfArgs(this_expected_call),
                NumberOfArgs(this_actual_call)
                );
        call_number++;
        expected_calls  = expected_calls->next;
        actual_calls    = actual_calls->next;
    }
}
static void print_first_unexpected_call(Mock_s *self)
{   // Calls made by the function under test, not expected by the test.
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    uint8_t num_expected_calls = g_list_length(expected_calls);
    uint8_t num_actual_calls   = g_list_length(actual_calls);
    if (num_expected_calls < num_actual_calls)
        g_string_append_printf( self->fail_msg,
            "First unexpected call: received #%d:'%s'. ",
            num_expected_calls + 1,
            ((RecordedCall *)g_list_nth_data(
                actual_calls, num_expected_calls)
                )->name
            );
}
static void print_first_missed_call(Mock_s *self)
{   // Calls expected by the test, not made by the function under test.
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    uint8_t num_expected_calls = g_list_length(expected_calls);
    uint8_t num_actual_calls   = g_list_length(actual_calls);
    if (num_expected_calls > num_actual_calls)
        g_string_append_printf( self->fail_msg,
            "First missed call: expected #%d:'%s'. ",
            num_actual_calls + 1,
            ((RecordedCall *)g_list_nth_data(
                expected_calls, num_actual_calls)
            )->name
            //(gchar *)g_list_nth_data(
            //    expected_calls,
            //    num_actual_calls
            //    )
            );
}
static void PrintExpectedAndActualNames( GString *fail_msg,
    uint8_t call_number, char const *expected_name, char const *actual_name)
{
    g_string_append_printf( fail_msg,
        "Call #%d: expected '%s', was '%s'. ",
        call_number, expected_name, actual_name);
}
static void PrintExpectedAndActualInputs(GString *fail_msg,
    uint8_t call_number, GList *expected_inputs, GList *actual_inputs)
{   // loop through the inputs and print the non-matching
    GString *printed_expected_arg = g_string_new(
    "Error: `RecordedArg->Print` should have overwritten this message."
    );
    GString *printed_actual_arg = g_string_new(
    "Error: `RecordedArg->Print` should have overwritten this message."
    );
    while (expected_inputs != NULL && actual_inputs != NULL)
    {
        RecordedArg *this_expected_arg = (RecordedArg *)expected_inputs->data;
        RecordedArg *this_actual_arg   = (RecordedArg *)actual_inputs->data;
        if (!RecordedArgsMatch(
            this_expected_arg,
            this_actual_arg))
        {//print them;
            this_expected_arg->Print(   // print to the GString
                printed_expected_arg, this_expected_arg);
            this_actual_arg->Print(   // print to the GString
                printed_actual_arg, this_actual_arg);
            g_string_append_printf(fail_msg,
                "Call #%d: expected '%s', was '%s'. ",
                call_number,
                printed_expected_arg->str,
                printed_actual_arg->str);
        }
        expected_inputs = expected_inputs->next;
        actual_inputs   = actual_inputs->next;
    }
    g_string_free(printed_expected_arg, true);
    g_string_free(printed_actual_arg, true);
}
static void print_all_wrong_calls(Mock_s *self)
{
    uint8_t call_number = 1;
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    while (expected_calls!=NULL && actual_calls!=NULL)
    {
        RecordedCall *this_expected_call = (RecordedCall *)expected_calls->data;
        RecordedCall *this_actual_call   = (RecordedCall *)actual_calls->data;
        if (!CallNamesMatch(
            this_expected_call->name,
            this_actual_call->name)
            ) PrintExpectedAndActualNames(
                self->fail_msg,
                call_number,
                this_expected_call->name,
                this_actual_call->name);
        if (!CallInputsMatch(
            this_expected_call->inputs,
            this_actual_call->inputs)
            ) PrintExpectedAndActualInputs(
                self->fail_msg,
                call_number,
                this_expected_call->inputs,
                this_actual_call->inputs);

        expected_calls = expected_calls->next;
        actual_calls = actual_calls->next;
        call_number++;
    }
}
