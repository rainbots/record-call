#include <stdio.h>      // printf
#include <stdlib.h>     // malloc, free
#include <glib.h>       // GString, GList
#include <stdbool.h>    // bool, true, false


//=====[ mock-c RecordedArg datatype ]=====
typedef struct RecordedArg RecordedArg;
typedef void (SetupRecordedArg)(RecordedArg *self);
static SetupRecordedArg     SetupRecord_uint8;
static SetupRecordedArg     SetupRecord_uint16;
static SetupRecordedArg     SetupRecord_GString;
typedef void (FreeArg)(RecordedArg *self);
static FreeArg              Destroy_uint8;
static FreeArg              Destroy_uint16;
static FreeArg              Destroy_GString;
typedef void (PrintArgToString)(GString *string, RecordedArg *self);
static PrintArgToString     Print_uint8;
static PrintArgToString     Print_uint16;
static PrintArgToString     Print_GString;
struct RecordedArg
{
    void *pArg;    // ex: *((uint8_t *)record_of_arg->pArg) returns a uint8_t
    // Store functions for working with the value on the heap pArg points to.
    // The function pointers are assigned by a SetupRecordedArg function.
    FreeArg             *Destroy;
    PrintArgToString    *Print;
    // Anything else needed by mock-c goes here.
};
RecordedArg * RecordedArg_new(SetupRecordedArg SetupRecord)
{
    RecordedArg *out = (RecordedArg *)malloc(sizeof(RecordedArg));
    SetupRecord(out);
    return out;
}
void RecordedArg_destroy(RecordedArg *self)
{   // Deallocate this recorded arg.
    self->Destroy(self);        // Deallocate the memory `pArg` points to.
                                // `Destroy` is assigned to a function specific
                                // to the datatype of this recorded arg.
    free(self);                 // Deallocate the RecordedArg struct itself.
}

//=====[ mock-c RecordedCall datatype ]=====
typedef struct RecordedCall RecordedCall;
struct RecordedCall {
    char const * name;
    GList *inputs;    // The GList must be initialized to NULL.
};
RecordedCall * RecordedCall_new(char const *name)
{
    RecordedCall *out = (RecordedCall *)malloc(sizeof(RecordedCall));
    out->name = name;
    out->inputs = NULL;
    return out;
}
void RecordedCall_destroy(RecordedCall *self)
{   // Deallocate this recorded call.
    g_list_free_full(           // Deallocate each GList struct and free the
        self->inputs,           // RecordedArg that each data member points to.
        (GDestroyNotify)RecordedArg_destroy
        );
    free(self);                 // Deallocate the RecordedCall struct itself.
}
void RecordArg(RecordedCall *self, RecordedArg *input_arg)
{
    self->inputs = g_list_append(
        self->inputs,
        (RecordedArg *)input_arg
        );
}

//=====[ mock-c Mock datatype ]=====
typedef struct Mock_s Mock_s;
struct Mock_s {
    GString *fail_msg;
    GList *expected_calls;    // The GList must be initialized to NULL.
    GList *actual_calls;    // The GList must be initialized to NULL.
};
Mock_s * Mock_new(void)
{   // Initialize all pointer members!
    Mock_s *out = (Mock_s *)malloc(sizeof(Mock_s));
    out->fail_msg = g_string_new(
        "No failures recorded. This message should never print."
        );
    out->expected_calls = NULL;
    out->actual_calls   = NULL;
    return out;
}
void Mock_destroy(Mock_s *self)
{   // Deallocate this mock struct.
    g_list_free_full(           // Deallocate each GList struct and free the
        self->expected_calls,   // RecordedCall that each data member points to.
        (GDestroyNotify)RecordedCall_destroy);
    g_list_free_full(
        self->actual_calls,
        (GDestroyNotify)RecordedCall_destroy);
    g_string_free(              // Deallocate the GString struct 
        self->fail_msg, true);  // and the message string it points to.
    free(self);  // Tabled: I cannot tell if this is deallocating the Mock_s.
}
void RecordExpectedCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of expected calls.
    self->expected_calls = g_list_append(
        self->expected_calls,
        (RecordedCall *)func_call
        );
}
void RecordActualCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of actual calls.
    self->actual_calls = g_list_append(
        self->actual_calls,
        (RecordedCall *)func_call
        );
}

//=====[ Examples using above datatypes to implement mock-c functionalty ]=====
static void PrintAllInputs(GList *inputs)
{
    GString *printed_arg = g_string_new(
        "Error: `RecordedArg->Print` should have overwritten this message."
        );
    while (inputs != NULL)
    {
        RecordedArg *this_arg = (RecordedArg *)inputs->data;
        (this_arg->Print(printed_arg, this_arg));   // print to the GString
        printf(", %s", printed_arg->str);           // print to stdout
        //printf(", %s", ((AnyType_s *)inputs->data)->printed->str);
        inputs = inputs->next;
    }
    printf("\n");
    g_string_free(printed_arg, true);
}
void PrintAllCalls(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    //while (expected_calls != NULL && actual_calls != NULL)
    printf("Expected calls:\n");
    while (expected_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)expected_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        expected_calls  = expected_calls->next;
    }
    printf("Actual calls:\n");
    while (actual_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)actual_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        actual_calls  = actual_calls->next;
    }
}

//=====[ Client code ]=====
Mock_s *mock;

//=====[ Mock_DOF, Expect_DOF, and DOF_Stubbed to be Vim-scripted ]=====
// The Vim shortcut acts like a macro, giving me the code to paste in.
// These Expect_DOF and DOF_Stubbed functions are
//      *incredibly similar but not identical for each datatype*
// - no args -- has no mention of RecordArg, does not append to inputs.
// - GString -- gets heap mem by deep-copying the GString
//
// - 8BitArg and 16BitArg are identical
// - even Two8bitArgs could probably go under the same script.

RecordedCall * Mock_TakesNoArg(void)
{
    char const *call_name = "TakesNoArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    return record_of_call;
}
void Expect_TakesNoArg(void) {
    RecordExpectedCall(mock, Mock_TakesNoArg());
}
void TakesNoArg_Stubbed(void) {
    RecordActualCall(mock, Mock_TakesNoArg());
}

RecordedCall * Mock_Takes8bitArg(uint8_t My8BitArg)
{
    char const *call_name = "Takes8bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_Takes8bitArg(uint8_t My8BitArg) {
    RecordExpectedCall(mock, Mock_Takes8bitArg(My8BitArg));
}
void Takes8bitArg_Stubbed(uint8_t My8BitArg) {
    RecordActualCall(mock, Mock_Takes8bitArg(My8BitArg));
}

RecordedCall * Mock_TakesTwo8bitArgs(uint8_t b1, uint8_t b2)
{
    char const *call_name = "TakesTwo8bitArgs";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = b1;
    RecordedArg *record_of_arg2 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg2->pArg) = b2;
    RecordArg(record_of_call, record_of_arg1);
    RecordArg(record_of_call, record_of_arg2);
    return record_of_call;
}
void Expect_TakesTwo8bitArgs(uint8_t b1, uint8_t b2) {
    RecordExpectedCall(mock, Mock_TakesTwo8bitArgs(b1, b2));
}
void TakesTwo8bitArgs_Stubbed(uint8_t b1, uint8_t b2) {
    RecordActualCall(mock, Mock_TakesTwo8bitArgs(b1, b2));
}
RecordedCall * Mock_Takes16bitArg(uint16_t My16bitArg)
{
    char const *call_name = "Takes16bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint16);
    *((uint16_t *)record_of_arg1->pArg) = My16bitArg;
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_Takes16bitArg(uint16_t My16bitArg){
    RecordExpectedCall(mock, Mock_Takes16bitArg(My16bitArg));
}
void Takes16bitArg_Stubbed(uint16_t My16bitArg){
    RecordActualCall(mock, Mock_Takes16bitArg(My16bitArg));
}

RecordedCall * Mock_TakesGStringPtr(GString *pGString)
{
    char const *call_name = "TakesGStringPtr";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_GString);
    record_of_arg1->pArg = (void *)g_string_new(pGString->str);
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_TakesGStringPtr(GString *pGString){
    RecordExpectedCall(mock, Mock_TakesGStringPtr(pGString));
}
void TakesGStringPtr_Stubbed(GString *pGString){
    RecordActualCall(mock, Mock_TakesGStringPtr(pGString));
}

int main()
{
    printf("# Recording function calls that take input args.\n");
    mock = Mock_new();
    //=====[ Generate the list of expected calls ]=====
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    Expect_TakesTwo8bitArgs(0xaa,0xbb);
    Expect_Takes16bitArg(0x1234);
    GString *pExpectedMsg = g_string_new("the expected message");
    Expect_TakesGStringPtr(pExpectedMsg);
    g_string_free(pExpectedMsg, true);
    //=====[ Simulate stubbed DOF calls that happen when FUT is called ]=====
    TakesNoArg_Stubbed();
    Takes8bitArg_Stubbed(0x21);
    TakesTwo8bitArgs_Stubbed(0xcc,0xdd);
    Takes16bitArg_Stubbed(0x5678);
    GString *pActualMsg = g_string_new("the actual message received");
    TakesGStringPtr_Stubbed(pActualMsg);
    g_string_free(pActualMsg, true);
    //=====[ Walk the lists of calls ]=====
    PrintAllCalls(mock);
    //=====[ Cleanup ]=====
    Mock_destroy(mock);
    mock = NULL;
}

//=====[ RecordedArg: Define SetupRecord for different arg datatypes ]=====
static void SetupRecord_uint8(RecordedArg *self)
{
    self->pArg = malloc(sizeof(uint8_t));           // Get heap-mem.
    self->Destroy = Destroy_uint8;                  // How to deallocate.
    self->Print   = Print_uint8;                    // How to print.
}
static void SetupRecord_uint16(RecordedArg *self)
{
    self->pArg = malloc(sizeof(uint16_t));          // Get heap-mem.
    self->Destroy = Destroy_uint16;                 // How to deallocate.
    self->Print   = Print_uint16;                   // How to print.
}
static void SetupRecord_GString(RecordedArg *self)
{
    self->pArg = malloc(sizeof(GString));           // Get heap-mem.
    self->Destroy = Destroy_GString;                // How to deallocate.
    self->Print   = Print_GString;                  // How to print.
}
//=====[ RecordedArg: Define Destroy for different arg datatypes ]=====
static void Destroy_uint8(RecordedArg *self)
{
    free((uint8_t *)self->pArg);
}
static void Destroy_uint16(RecordedArg *self)
{
    free((uint16_t *)self->pArg);
}
static void Destroy_GString(RecordedArg *self)
{
    g_string_free((GString *)self->pArg, true);
}

//=====[ RecordedArg: Define Print for different arg datatypes ]=====
static void Print_uint8(GString *string, RecordedArg *self)
{   // Caller provides a temporary string to store the result in.
    g_string_printf(string, "%#04x", *(uint8_t *)self->pArg);
}
static void Print_uint16(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%#06x", *(uint16_t *)self->pArg);
    //g_string_printf(string, "%d", *(uint16_t *)self->pArg);
}
static void Print_GString(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%s", ((GString *)self->pArg)->str);
}
