#include <stdio.h>      // printf
#include <stdlib.h>     // malloc, free
#include <glib.h>       // GString, GList
#include <stdbool.h>    // bool, true, false
#include "Mock.h"
#include "RecordedCall.h"
#include "RecordedArg.h"
#include "record-calls.h"


//=====[ Client code ]=====
Mock_s *mock;
void NoArgsMatch(void)
{
    printf("# Recording function calls that take input args.\n");
    mock = Mock_new();
    //=====[ Generate the list of expected calls ]=====
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    Expect_TakesTwo8bitArgs(0xaa,0xbb);
    Expect_Takes16bitArg(0x1234);
    GString *pExpectedMsg = g_string_new("the expected message");
    Expect_TakesGStringPtr(pExpectedMsg);
    g_string_free(pExpectedMsg, true);
    //=====[ Simulate stubbed DOF calls that happen when FUT is called ]=====
    TakesNoArg_Stubbed();
    Takes8bitArg_Stubbed(0x21);
    TakesTwo8bitArgs_Stubbed(0xcc,0xdd);
    Takes16bitArg_Stubbed(0x5678);
    GString *pActualMsg = g_string_new("the actual message received");
    TakesGStringPtr_Stubbed(pActualMsg);
    g_string_free(pActualMsg, true);
    //=====[ Walk the lists of calls ]=====
    PrintAllCalls(mock);
    if (RanAsHoped(mock)) printf("\n---Ran as hoped.---\n");
    else printf("\n---Why it failed---\n%s", WhyDidItFail(mock));
    //=====[ Cleanup ]=====
    Mock_destroy(mock);
    mock = NULL;
}
void AllArgsMatch(void)
{
    printf("# Recording function calls that take input args.\n");
    mock = Mock_new();
    //=====[ Generate the list of expected calls ]=====
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    Expect_TakesTwo8bitArgs(0xaa,0xbb);
    Expect_Takes16bitArg(0x1234);
    GString *pExpectedMsg = g_string_new("the message");
    Expect_TakesGStringPtr(pExpectedMsg);
    g_string_free(pExpectedMsg, true);
    //=====[ Simulate stubbed DOF calls that happen when FUT is called ]=====
    TakesNoArg_Stubbed();
    Takes8bitArg_Stubbed(0x12);
    TakesTwo8bitArgs_Stubbed(0xaa,0xbb);
    Takes16bitArg_Stubbed(0x1234);
    GString *pActualMsg = g_string_new("the message");
    TakesGStringPtr_Stubbed(pActualMsg);
    g_string_free(pActualMsg, true);
    //=====[ Walk the lists of calls ]=====
    PrintAllCalls(mock);
    if (RanAsHoped(mock)) printf("\n---Ran as hoped.---\n");
    else printf("\n---Why it failed---\n%s", WhyDidItFail(mock));
    //=====[ Cleanup ]=====
    Mock_destroy(mock);
    mock = NULL;
}
void SomeArgsMatch(void)
{
    printf("# Recording function calls that take input args.\n");
    mock = Mock_new();
    //=====[ Generate the list of expected calls ]=====
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    Expect_TakesTwo8bitArgs(0xaa,0xbb);
    Expect_Takes16bitArg(0x1234);
    GString *pExpectedMsg = g_string_new("the expected message");
    Expect_TakesGStringPtr(pExpectedMsg);
    g_string_free(pExpectedMsg, true);
    //=====[ Simulate stubbed DOF calls that happen when FUT is called ]=====
    TakesNoArg_Stubbed();
    Takes8bitArg_Stubbed(0x12);
    TakesTwo8bitArgs_Stubbed(0xcc,0xdd);
    Takes16bitArg_Stubbed(0x1234);
    GString *pActualMsg = g_string_new("the actual message received");
    TakesGStringPtr_Stubbed(pActualMsg);
    g_string_free(pActualMsg, true);
    //=====[ Walk the lists of calls ]=====
    PrintAllCalls(mock);
    if (RanAsHoped(mock)) printf("\n---Ran as hoped.---\n");
    else printf("\n---Why it failed---\n%s", WhyDidItFail(mock));
    //=====[ Cleanup ]=====
    Mock_destroy(mock);
    mock = NULL;
}
int main()
{
    //NoArgsMatch();
    //AllArgsMatch();
    SomeArgsMatch();
}

//=====[ Mock_DOF, Expect_DOF, and DOF_Stubbed to be Vim-scripted ]=====
    /* The Vim shortcut acts like a macro, giving me the code to paste in.
     * These Expect_DOF and DOF_Stubbed functions are
     *      *incredibly similar but not identical for each datatype*
     * - no args -- has no mention of RecordArg, does not append to inputs.
     * - GString -- gets heap mem by deep-copying the GString
     *
     * - 8BitArg and 16BitArg are identical
     * - even Two8bitArgs could probably go under the same script.
     */
RecordedCall * Mock_TakesNoArg(void)
{
    char const *call_name = "TakesNoArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    return record_of_call;
}
void Expect_TakesNoArg(void) {
    RecordExpectedCall(mock, Mock_TakesNoArg());
}
void TakesNoArg_Stubbed(void) {
    RecordActualCall(mock, Mock_TakesNoArg());
}

RecordedCall * Mock_Takes8bitArg(uint8_t My8BitArg)
{
    char const *call_name = "Takes8bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_Takes8bitArg(uint8_t My8BitArg) {
    RecordExpectedCall(mock, Mock_Takes8bitArg(My8BitArg));
}
void Takes8bitArg_Stubbed(uint8_t My8BitArg) {
    //RecordActualCall(mock, Mock_Takes8bitArg(My8BitArg));
    char const *call_name = "Takes8bitArg_vStub";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = My8BitArg;
    RecordArg(record_of_call, record_of_arg1);
    RecordActualCall(mock, record_of_call);
}

RecordedCall * Mock_TakesTwo8bitArgs(uint8_t b1, uint8_t b2)
{
    char const *call_name = "TakesTwo8bitArgs";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = b1;
    RecordedArg *record_of_arg2 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg2->pArg) = b2;
    RecordArg(record_of_call, record_of_arg1);
    RecordArg(record_of_call, record_of_arg2);
    return record_of_call;
}
void Expect_TakesTwo8bitArgs(uint8_t b1, uint8_t b2) {
    RecordExpectedCall(mock, Mock_TakesTwo8bitArgs(b1, b2));
}
void TakesTwo8bitArgs_Stubbed(uint8_t b1, uint8_t b2) {
    RecordActualCall(mock, Mock_TakesTwo8bitArgs(b1, b2));
}

RecordedCall * Mock_Takes16bitArg(uint16_t My16bitArg)
{
    char const *call_name = "Takes16bitArg";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint16);
    *((uint16_t *)record_of_arg1->pArg) = My16bitArg;
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_Takes16bitArg(uint16_t My16bitArg){
    RecordExpectedCall(mock, Mock_Takes16bitArg(My16bitArg));
}
void Takes16bitArg_Stubbed(uint16_t My16bitArg){
    RecordActualCall(mock, Mock_Takes16bitArg(My16bitArg));
}

RecordedCall * Mock_TakesGStringPtr(GString *pGString)
{
    char const *call_name = "TakesGStringPtr";
    RecordedCall *record_of_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_GString);
    record_of_arg1->pArg = (void *)g_string_new(pGString->str);
    RecordArg(record_of_call, record_of_arg1);
    return record_of_call;
}
void Expect_TakesGStringPtr(GString *pGString){
    RecordExpectedCall(mock, Mock_TakesGStringPtr(pGString));
}
void TakesGStringPtr_Stubbed(GString *pGString){
    RecordActualCall(mock, Mock_TakesGStringPtr(pGString));
}

