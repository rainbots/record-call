#include <stdio.h>      // printf
#include <stdlib.h>     // malloc, free
#include <glib.h>       // GString, GList
#include <stdbool.h>    // bool, true, false


//=====[ mock-c RecordedArg datatype ]=====
typedef struct RecordedArg RecordedArg;
typedef void (FreeArg)(RecordedArg *self);
static FreeArg             Destroy_uint8;
static FreeArg             Destroy_uint16;
static FreeArg             Destroy_GString;
typedef void (PrintArgToString)(GString *string, RecordedArg *self);
static PrintArgToString    Print_uint8;
static PrintArgToString    Print_uint16;
static PrintArgToString    Print_GString;
struct RecordedArg
{
    void *pArg;    // *(uint8_t *)any_type->pArg is a uint8_t
    // TODO:
    // Store functions for working with the value on the heap pArg points to.
    FreeArg             *Destroy;
    PrintArgToString    *Print;
    // Anything else needed by mock-c goes here.
};
// TODO: require Destroy method to be passed in as well.
RecordedArg * RecordedArg_new(void *pArg)
{
    RecordedArg *out = (RecordedArg *)malloc(sizeof(RecordedArg));
    out->pArg = pArg;
    out->Destroy = NULL;
    out->Print = NULL;
    return out;
}
void RecordedArg_destroy(RecordedArg *self)
{   // Deallocate this recorded arg.
    self->Destroy(self);        // Deallocate the memory `pArg` points to.
                                // `Destroy` is assigned to a function specific
                                // to the datatype of this recorded arg.
    free(self);                 // Deallocate the RecordedArg struct itself.
}


//=====[ mock-c RecordedCall datatype ]=====
typedef struct RecordedCall RecordedCall;
struct RecordedCall {
    gchar const * name;
    GList *inputs;    // The GList must be initialized to NULL.
};
RecordedCall * RecordedCall_new(void)
{
    RecordedCall *out = (RecordedCall *)malloc(sizeof(RecordedCall));
    return out;
}
void RecordedCall_destroy(RecordedCall *self)
{   // Deallocate this recorded call.
    g_list_free_full(           // Deallocate each GList struct and free the
        self->inputs,           // RecordedArg that each data member points to.
        (GDestroyNotify)RecordedArg_destroy
        );
    free(self);                 // Deallocate the RecordedCall struct itself.
}

//=====[ mock-c Mock datatype ]=====
typedef struct Mock_s Mock_s;
struct Mock_s {
    GString *fail_msg;
    GList *expected_calls;    // The GList must be initialized to NULL.
    GList *actual_calls;    // The GList must be initialized to NULL.
};
Mock_s * Mock_new(void)
{   // Initialize all pointer members!
    Mock_s *out = (Mock_s *)malloc(sizeof(Mock_s));
    out->fail_msg = g_string_new(
        "No failures recorded. This message should never print."
        );
    out->expected_calls = NULL;
    out->actual_calls   = NULL;
    return out;
}
void Mock_destroy(Mock_s *self)
{   // Deallocate this mock struct.
    g_list_free_full(           // Deallocate each GList struct and free the
        self->expected_calls,   // RecordedCall that each data member points to.
        (GDestroyNotify)RecordedCall_destroy);
    g_list_free_full(
        self->actual_calls,
        (GDestroyNotify)RecordedCall_destroy);
    g_string_free(              // Deallocate the GString struct 
        self->fail_msg, true);  // and the message string it points to.
    free(self);  // Tabled: I cannot tell if this is deallocating the Mock_s.
}
void RecordExpectedCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of expected calls.
    self->expected_calls = g_list_append(
        self->expected_calls,
        (RecordedCall *)func_call
        );
}
void RecordActualCall(Mock_s *self, RecordedCall *func_call)
{   // Record a function call in the list of actual calls.
    self->actual_calls = g_list_append(
        self->actual_calls,
        (RecordedCall *)func_call
        );
}

//=====[ Examples using above datatypes to implement mock-c functionalty ]=====
static void PrintAllInputs(GList *inputs)
{
    GString *printed_arg = g_string_new(
        "Error: `RecordedArg->Print` should have overwritten this message."
        );
    while (inputs != NULL)
    {
        RecordedArg *this_arg = (RecordedArg *)inputs->data;
        (this_arg->Print(printed_arg, this_arg));   // print to the GString
        printf(", %s", printed_arg->str);           // print to stdout
        //printf(", %s", ((AnyType_s *)inputs->data)->printed->str);
        inputs = inputs->next;
    }
    printf("\n");
    g_string_free(printed_arg, true);
}
void PrintAllCalls(Mock_s *self)
{
    GList *expected_calls   = self->expected_calls;
    GList *actual_calls     = self->actual_calls;
    //while (expected_calls != NULL && actual_calls != NULL)
    printf("Expected calls:\n");
    while (expected_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)expected_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        expected_calls  = expected_calls->next;
    }
    printf("Actual calls:\n");
    while (actual_calls != NULL) {
        RecordedCall *this_call = (RecordedCall *)actual_calls->data;
        printf( "    \"%s\"", this_call->name);
        PrintAllInputs(this_call->inputs);
        actual_calls  = actual_calls->next;
    }
}

//=====[ Client code ]=====
Mock_s *mock;

//=====[ Expect_DOF and DOF_Stubbed to be Vim scripted ]=====
// The Vim shortcut acts like a macro, giving me the code to paste in.
// These Expect_DOF and DOF_Stubbed functions are
//      *incredibly similar but not identical for each datatype*
// - no args -- has no mention of RecordArg, does not append to inputs.
// - GString -- gets heap mem by deep-copying the GString
//
// - 8BitArg and 16BitArg are identical
// - even Two8bitArgs could probably go under the same script.
void Expect_TakesNoArg(void)
{
    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesNoArg";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.

    RecordExpectedCall(mock, pRCall);  // appends to mock->expected_calls
    // mock->expected_calls->data now aliases pRCall.
}
void TakesNoArg_Stubbed(void)
{
    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesNoArg_vStub";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.

    RecordActualCall(mock, pRCall);  // appends to mock->actual_calls
    // mock->actual_calls->data now aliases pRCall.
}

void Expect_Takes8bitArg(uint8_t My8BitArg)
{
    // Record the inputs passed into the stubbed function.
    uint8_t *pArg1 = (uint8_t *)malloc(sizeof(My8BitArg)); // Get heap-mem.
    *pArg1 = My8BitArg;                                    // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg1->Print   = Print_uint8;                        // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "Takes8bitArg";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordExpectedCall(mock, pRCall);  // appends to mock->expected_calls
    // mock->expected_calls->data now aliases pRCall.
}
void Takes8bitArg_Stubbed(uint8_t My8BitArg)
{
    // Record the inputs passed into the stubbed function.
    uint8_t *pArg1 = (uint8_t *)malloc(sizeof(My8BitArg)); // Get heap-mem.
    *pArg1 = My8BitArg;                                    // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg1->Print   = Print_uint8;                        // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "Takes8bitArg_vStub";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordActualCall(mock, pRCall);  // appends to mock->actual_calls
    // mock->actual_calls->data now aliases pRCall.
}

void Expect_TakesTwo8bitArgs(uint8_t My8BitArg1, uint8_t My8BitArg2)
{
    // Record the inputs passed into the stubbed function.
    // Arg1:
    uint8_t *pArg1 = (uint8_t *)malloc(sizeof(My8BitArg1)); // Get heap-mem.
    *pArg1 = My8BitArg1;                                    // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg1->Print   = Print_uint8;                        // How to print.
    // Arg2:
    uint8_t *pArg2 = (uint8_t *)malloc(sizeof(My8BitArg2)); // Get heap-mem.
    *pArg2 = My8BitArg2;                                    // Get arg value.
    RecordedArg *pRArg2 = RecordedArg_new(                // Create record.
        (void *)pArg2                                     // Alias heap-mem ptr.
        );
    pRArg2->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg2->Print   = Print_uint8;                        // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesTwo8bitArgs";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg1.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg2); // Append arg2.

    RecordExpectedCall(mock, pRCall);  // appends to mock->expected_calls
    // mock->expected_calls->data now aliases pRCall.
}
void TakesTwo8bitArgs_Stubbed(uint8_t My8BitArg1, uint8_t My8BitArg2)
{
    // Record the inputs passed into the stubbed function.
    // Arg1:
    uint8_t *pArg1 = (uint8_t *)malloc(sizeof(My8BitArg1)); // Get heap-mem.
    *pArg1 = My8BitArg1;                                    // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg1->Print   = Print_uint8;                        // How to print.
    // Arg2:
    uint8_t *pArg2 = (uint8_t *)malloc(sizeof(My8BitArg2)); // Get heap-mem.
    *pArg2 = My8BitArg2;                                    // Get arg value.
    RecordedArg *pRArg2 = RecordedArg_new(                // Create record.
        (void *)pArg2                                     // Alias heap-mem ptr.
        );
    pRArg2->Destroy = Destroy_uint8;                      // How to deallocate.
    pRArg2->Print   = Print_uint8;                        // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesTwo8bitArgs_vStub";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg1.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg2); // Append arg2.

    RecordActualCall(mock, pRCall);  // appends to mock->actual_calls
    // mock->actual_calls->data now aliases pRCall.
}

void Expect_Takes16bitArg(uint16_t My16BitArg)
{
    // Record the inputs passed into the stubbed function.
    uint16_t *pArg1 = (uint16_t *)malloc(sizeof(My16BitArg)); // Get heap-mem.
    *pArg1 = My16BitArg;                                  // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint16;                     // How to deallocate.
    pRArg1->Print   = Print_uint16;                       // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "Takes16bitArg";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordExpectedCall(mock, pRCall);  // appends to mock->expected_calls
    // mock->expected_calls->data now aliases pRCall.
}
void Takes16bitArg_Stubbed(uint16_t My16BitArg)
{
    // Record the inputs passed into the stubbed function.
    uint16_t *pArg1 = (uint16_t *)malloc(sizeof(My16BitArg)); // Get heap-mem.
    *pArg1 = My16BitArg;                                  // Get arg value.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_uint16;                     // How to deallocate.
    pRArg1->Print   = Print_uint16;                       // How to print.

    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "Takes16bitArg_vStub";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordActualCall(mock, pRCall);  // appends to mock->actual_calls
    // mock->actual_calls->data now aliases pRCall.
}

void Expect_TakesGStringPtr(GString * pGString)
{
    // Record the inputs passed into the stubbed function.
    GString *pArg1 = g_string_new(pGString->str);         // Get heap-mem and
                                                          // the string itself.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_GString;                  // How to deallocate.
    pRArg1->Print   = Print_GString;                    // How to print.
    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesGStringPtr";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordExpectedCall(mock, pRCall);  // appends to mock->expected_calls
    // mock->expected_calls->data now aliases pRCall.
}
void TakesGStringPtr_Stubbed(GString * pGString)
{
    // Record the inputs passed into the stubbed function.
    // Because this points to something already on the heap, I need to make a
    // copy of it, not just alias the heap-mem. If I just alias the heap-mem,
    // then the original allocator of the heap-mem is going to destroy it and I
    // am going to destroy it again when I destroy the mock where this record
    // lives. I cannot destroy it twice. Is there any harm in destroying it
    // twice? Probably. Whatever, copying the string is easy.

    GString *pArg1 = g_string_new(pGString->str);         // Get heap-mem and
                                                          // the string itself.
    RecordedArg *pRArg1 = RecordedArg_new(                // Create record.
        (void *)pArg1                                     // Alias heap-mem ptr.
        );
    pRArg1->Destroy = Destroy_GString;                  // How to deallocate.
    pRArg1->Print   = Print_GString;                    // How to print.
    // Record that the function was called.
    RecordedCall *pRCall = RecordedCall_new();              // Get heap-mem.
    pRCall->name = "TakesGStringPtr_vStub";  // points at static mem
    pRCall->inputs = NULL;                                  // Init arg list.
    pRCall->inputs = g_list_append(pRCall->inputs, pRArg1); // Append arg.
    //pRCall->inputs = g_list_append(pRCall->inputs, pRArg2);

    RecordActualCall(mock, pRCall);  // appends to mock->actual_calls
    // mock->actual_calls->data now aliases pRCall.
}

int main()
{
    printf("# Recording function calls that take input args.\n");
    mock = Mock_new();
    //=====[ Generate the list of expected calls ]=====
    Expect_TakesNoArg();
    Expect_Takes8bitArg(0x12);
    Expect_TakesTwo8bitArgs(0xaa,0xbb);
    Expect_Takes16bitArg(0x1234);
    GString *pExpectedMsg = g_string_new("the expected message");
    Expect_TakesGStringPtr(pExpectedMsg);
    g_string_free(pExpectedMsg, true);
    //=====[ Simulate stubbed DOF calls that happen when FUT is called ]=====
    TakesNoArg_Stubbed();
    Takes8bitArg_Stubbed(0x21);
    TakesTwo8bitArgs_Stubbed(0xcc,0xdd);
    Takes16bitArg_Stubbed(0x5678);
    GString *pActualMsg = g_string_new("the actual message received");
    TakesGStringPtr_Stubbed(pActualMsg);
    g_string_free(pActualMsg, true);
    //=====[ Walk the lists of calls ]=====
    PrintAllCalls(mock);
    //=====[ Cleanup ]=====
    Mock_destroy(mock);
    mock = NULL;
}

//=====[ RecordedArg: Define Destroy for different arg datatypes ]=====
static void Destroy_uint8(RecordedArg *self)
{
    free((uint8_t *)self->pArg);
}
static void Destroy_uint16(RecordedArg *self)
{
    free((uint16_t *)self->pArg);
}
static void Destroy_GString(RecordedArg *self)
{
    g_string_free((GString *)self->pArg, true);
}

//=====[ RecordedArg: Define Print for different arg datatypes ]=====
static void Print_uint8(GString *string, RecordedArg *self)
{   // Caller provides a temporary string to store the result in.
    g_string_printf(string, "%#04x", *(uint8_t *)self->pArg);
}
static void Print_uint16(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%#06x", *(uint16_t *)self->pArg);
    //g_string_printf(string, "%d", *(uint16_t *)self->pArg);
}
static void Print_GString(GString *string, RecordedArg *self)
{
    g_string_printf(string, "%s", ((GString *)self->pArg)->str);
}
