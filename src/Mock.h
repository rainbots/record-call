#ifndef _MOCK_H
#define _MOCK_H

#include "RecordedCall.h"   // RecordedCall
#include "RecordedArg.h"  // RecordedArg
#include <stdbool.h>        // bool, true, false

//=====[ mock-c Mock datatype ]=====
typedef struct Mock_s Mock_s;
extern Mock_s *mock;    // memory allocated in the test runner
Mock_s * Mock_new(void);
void Mock_destroy(Mock_s *self);
void RecordExpectedCall(Mock_s *self, RecordedCall *func_call);
void RecordActualCall(Mock_s *self, RecordedCall *func_call);
void RecordArg(RecordedCall *self, RecordedArg *input_arg);
void PrintAllCalls(Mock_s *self);  // Example
// copied from old mock-c
bool RanAsHoped(Mock_s *self);
char const * WhyDidItFail(Mock_s *self);

#endif // _MOCK_H
