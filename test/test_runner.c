#include <unity.h>
#include <stdbool.h>
#include <test_Mock.h>
#include <Mock.h>

void (*setUp)(void); void (*tearDown)(void);
Mock_s *mock;

bool Yep=true, Nope=false;

void DevelopingMock(bool run_test) { if (run_test) {
    setUp       = SetUp_libMock;
    tearDown    = TearDown_libMock;
    RUN_TEST(RanAsHoped_returns_true_if_call_lists_match);
    RUN_TEST(RanAsHoped_returns_false_if_more_expected_calls_than_actual_calls);
    RUN_TEST(RanAsHoped_returns_false_if_more_actual_calls_than_expected_calls);
    RUN_TEST(RanAsHoped_returns_false_if_call_names_do_not_match);
    RUN_TEST(RanAsHoped_returns_false_if_a_call_expected_more_inputs);
    RUN_TEST(RanAsHoped_returns_false_if_a_call_expected_less_inputs);
    RUN_TEST(RanAsHoped_returns_false_if_a_call_has_the_wrong_input_value);
    RUN_TEST(WhyDidItFail_reports_all_actual_calls_that_were_never_expected);
    RUN_TEST(WhyDidItFail_reports_all_actual_calls_that_were_expected_but_out_of_order);
    }
}
void DevelopingRecordedCall(bool run_test) { if (run_test) { } }
void DevelopingRecordedArg(bool run_test) { if (run_test) { } }

int main(void)
{
    UNITY_BEGIN();
    DevelopingMock          (Yep);
    DevelopingRecordedArg   (Nope);
    DevelopingRecordedCall  (Nope);
    return UNITY_END();
}
