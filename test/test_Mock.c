#include "test_Mock.h"
#include <unity.h>
#include <Mock.h>
#include <ExampleCalls.h>

//=====[ List of tests: specifications of RanAsHoped ]=====
    // [x] Pass if call lists are the same length and all values match.
    // RanAsHoped returns true if:
    // [x] call lists are the same length and all values match
    // RanAsHoped returns false if:
    // [x] there are more expected calls than actual calls
    // [x] there are more actual calls than expected calls
    // RanAsHoped returns false if a call has:
    // [x] an expected call name that does not match the actual call name
    // [x] more expected inputs than actual inputs
    // [x] more actual inputs than expected inputs
    // [x] an expected input value that does not match the actual input value
    //
    // WhyDidItFail reports
    // [x] all actual calls that were never expected
    // [ ] all actual calls that were expected but came in the wrong order
    // [ ] first unexpected actual call after exhausting a shorter list of expected calls
    // [ ] first missed call after exahusting a shorter list of actual calls
    //

void SetUp_libMock(void){
    mock = Mock_new();
    //
}
void TearDown_libMock(void){
    //
    Mock_destroy(mock);
    mock = NULL;
}

void RanAsHoped_returns_true_if_call_lists_match(void)
{   //  [x] call lists are the same length and all values match
    Expect_TakesNoArg();    // test sets an expectation that a DOF is called
    TakesNoArg_Stubbed();   // simulate the FUT calling the stubbed DOF
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
void RanAsHoped_returns_false_if_more_expected_calls_than_actual_calls(void)
{   // [x] there are more expected calls than actual calls
    Expect_TakesNoArg();    // test sets an expectation that a DOF is called
    // simulate the FUT missing the call to the stubbed DOF
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}
void RanAsHoped_returns_false_if_more_actual_calls_than_expected_calls(void)
{   // [x] there are more actual calls than expected calls
    SpanishInquisition_Stubbed();  // simulate the FUT making an unexpected call
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}
void RanAsHoped_returns_false_if_call_names_do_not_match(void)
{   // [x] an expected call name that does not match the actual call name
    Expect_TakesNoArg();  // test sets an expectation that TakesNoArg is called
    SpanishInquisition_Stubbed();  // simulate the FUT calling something else
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}
void RanAsHoped_returns_false_if_a_call_expected_more_inputs(void)
{   // [x] more expected inputs than actual inputs
    Expect_TakesTwoArgs(0xa1, 0xa2);  // test sets an expectation this DOF takes two args
    TakesOneArg_StubbedWithCallName_TakesTwoArgs(0xa1);
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}
void RanAsHoped_returns_false_if_a_call_expected_less_inputs(void)
{   // [x] more actual inputs than expected inputs
    Expect_TakesOneArg(0xb1);  // test sets an expectation this DOF takes one arg
    TakesTwoArgs_StubbedWithCallName_TakesOneArg(0xb1, 0xb2);
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}
void RanAsHoped_returns_false_if_a_call_has_the_wrong_input_value(void)
{   // [x] an expected input value that does not match the actual input value
    Expect_TakesOneArg(0xAA);   // test set an expectation this DOF receives 0xAA
    TakesOneArg_Stubbed(0xFF);  // simulate FUT calling DOF with 0xFF instead
    TEST_ASSERT_FALSE(RanAsHoped(mock));
}

void WhyDidItFail_reports_all_actual_calls_that_were_never_expected(void)
{   // [x] all actual calls that were never expected
    Expect_TakesNoArg();           // test expects the FUT to call this DOF
    SpanishInquisition_Stubbed();  // simulate the FUT making an unexpected call
    TEST_ASSERT_FALSE(RanAsHoped(mock));
    TEST_ASSERT_EQUAL_STRING(
        "Why it failed:"
        " Call #1: expected 'TakesNoArg',"
        " was 'SpanishInquisition'."
        " ",
        WhyDidItFail(mock)
        );
}

void WhyDidItFail_reports_all_actual_calls_that_were_expected_but_out_of_order(void)
{   // [ ] all actual calls that were expected but came in the wrong order
    Expect_TakesNoArg();
    Expect_TakesOneArg(0x01);
    Expect_TakesTwoArgs(0x02,0x03);
    Expect_TakesOneArg(0xAA);
    TakesNoArg_Stubbed();           // simulate the FUT calling the expected DOF
    TakesTwoArgs_Stubbed(0x02,0x03);// simulate the FUT calling DOF out of order
    TakesOneArg_Stubbed(0x01);      // simulate the FUT calling DOF out of order
    TakesOneArg_Stubbed(0xAA);      // simulate the FUT calling the expected DOF
    TEST_ASSERT_FALSE(RanAsHoped(mock));
    TEST_ASSERT_EQUAL_STRING(
        "Why it failed:"
        " Wrong number of args in call #2 'TakesOneArg',"
        " expected 1, was 2."
        " Wrong number of args in call #3 'TakesTwoArgs',"
        " expected 2, was 1."
        " Call #2: expected 'TakesOneArg',"
        " was 'TakesTwoArgs'."
        " Call #2: expected '0x01', was '0x02'."
        " Call #3: expected 'TakesTwoArgs',"
        " was 'TakesOneArg'."
        " Call #3: expected '0x02', was '0x01'."
        " ",
        WhyDidItFail(mock)
        );
}
