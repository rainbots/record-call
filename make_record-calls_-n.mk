clang -c src/RecordedArg.c -o build/RecordedArg.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
clang -c src/RecordedCall.c -o build/RecordedCall.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
clang -c src/Mock.c -o build/Mock.o -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
clang src/record-calls.c build/RecordedArg.o build/RecordedCall.o build/Mock.o -o build/record-calls.exe -lglib-2.0 -lintl -L/usr/lib/glib-2.0 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -g -Wall -Wextra -pedantic \
	-I src/
build/record-calls.exe > build/record-calls.md #2> build/record-calls_stderr.md
